import { useEffect } from 'react';

export const scrollPageTop = () => {
  //fix this when route changes hook found
  useEffect(() => {
    if (typeof window !== 'undefined') {
      window.scrollTo(0, 0);
    }
  }, []);
};
