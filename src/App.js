import './App.css';
import { Route, Routes, HashRouter as BrowserRouter } from 'react-router-dom';
import Home from './pages/home';
import PL from './pages/filters';
import PDP from './pages/pdp';
import Checkout from './pages/checkout';
import Cart from './pages/cart';
import Success from './pages/success';
import NotFound from './pages/NotFound';

import Header from './components/header';
import Footer from './components/footer';
import { Fragment, useState } from 'react';
import { scrollPageTop } from './hooks/scrollTo';

function App() {
  const [openCart, setCartOpen] = useState(false);
  scrollPageTop();

  return (
    <div className='bg-white'>
      <BrowserRouter>
        <Header
          {...{
            openCart,
            setCartOpen,
          }}
        />
        <main>
          <Routes>
            <Route path='/' exact element={<Home />} />
            <Route path='/pl' exact element={<PL />} />
            <Route
              path='/pdp'
              exact
              element={
                <PDP
                  {...{
                    openCart,
                    setCartOpen,
                  }}
                />
              }
            />
            <Route path='/cart' exact element={<Cart />} />
            <Route path='/checkout' exact element={<Checkout />} />
            <Route path='/success' exact element={<Success />} />
            <Route path='*' element={<NotFound />} />
          </Routes>
        </main>
        <Footer />
      </BrowserRouter>
    </div>
  );
}

export default App;
