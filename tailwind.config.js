module.exports = {
  // ...
  content: ["./src/**/*.{html,js,ts,tsx}", "./index.html"],
  plugins: [
    // ...
    require('@tailwindcss/forms'),
    require('@tailwindcss/aspect-ratio'),
  ],
};
