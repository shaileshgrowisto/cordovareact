var tailwind = require('tailwindcss');
var autoprefixer = require('autoprefixer');
var tailwindConfig = require('./tailwind.config');

module.exports = {
  plugins: [tailwind(tailwindConfig), autoprefixer],
};
